import logging
import datetime

from flask import Flask
from flask_restplus import Resource, Api
import pandas_datareader as pdr
import pandas as pd

LOG = logging.getLogger(__name__)

app = Flask(__name__)
api = Api(app)

@api.route('/<string:ticker>')
class Price(Resource):
    def get(self, ticker):
        start_date = datetime.datetime.now() - datetime.timedelta(1)

        df = pdr.get_data_yahoo(ticker, start_date)[['Close','Open']]

        print('Ticker:' , ticker , df)

        df['Date'] = df.index

        return {'ticker': ticker,
                'date': df.iloc[-1]['Date'].strftime("%m/%d/%Y"),
                'open': df.iloc[-1]['Open'],
                'close': df.iloc[-1]['Close']}

#@api.route('/<string:ticker>/<string:startDate>/<string:endDate>')
#class Price(Resource):
#    def getWithDate(self, ticker,startDate,endDate):
#        df = pdr.get_data_yahoo(ticker, startDate)[['Close','Open']]


@api.route('/priceapi/faang')
class Faang(Resource):
    
    def get(self):
        faangString = 'Faang Method hit'

        fangprice = Price()

        df_faang = [fangprice.get('FB'),fangprice.get('AAPL'),fangprice.get('AMZN'),fangprice.get('NFLX'),fangprice.get('GOOG')]

        print(df_faang)
#conver to json format?
        return df_faang

if __name__ == '__main__':
    app.run(debug=True)
